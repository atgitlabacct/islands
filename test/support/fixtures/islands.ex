defmodule IslandsEngine.Fixtures.Islands do
  @moduledoc """
  Wraps Island creation with Coordinates for testing purposes.
  """
  alias IslandsEngine.{Coordinate, Island}

  @doc """
  Returns an island at Coordiante(2,2) by default.  You can also give an
  pass two args for row, col.
  """
  def square(), do: square(2, 2)

  def square(row, col) do
    {:ok, island} = Island.new(:square, Coordinate.new!(row, col))
    island
  end

  @doc """
  Returns an island at Coordiante(2,2) by default.  You can also give an
  pass two args for row, col.
  """
  def l_shape(row \\ 2, col \\ 2) do
    {:ok, island} = Island.new(:l_shape, Coordinate.new!(row, col))
    island
  end

  @doc """
  Returns a square and l_shape island in a list
  """
  def overlapping_islands() do
    {:ok, square} = Island.new(:square, Coordinate.new!(2, 2))
    {:ok, l_shape} = Island.new(:l_shape, Coordinate.new!(1, 2))
    [square, l_shape]
  end
end
