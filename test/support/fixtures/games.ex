defmodule IslandsEngine.Fixtures.Games do
  alias IslandsEngine.{Game}
  alias __MODULE__

  @player_keys [:player1, :player2]

  @moduledoc """
  Test fixtures for a Game
  """

  def game_with_players(p1, p2) when is_binary(p1) and is_binary(p2),
    do: Games.game_with_players(player1: p1, player2: p2)

  def game_with_players(opts \\ []) do
    check_options(opts)
    opts = options(opts)

    {:ok, game} = Game.new("test-game")

    game
    |> Game.add_player!(opts[:player1])
    |> Game.add_player!(opts[:player2])
  end

  def game_with_islands_set(opts \\ []) do
    game =
      opts
      |> options()
      |> game_with_players()

    Enum.reduce(@player_keys, game, fn p_key, g ->
      g
      |> Game.position_island(p_key, :atoll, 1, 1)
      |> Game.position_island(p_key, :dot, 1, 4)
      |> Game.position_island(p_key, :l_shape, 1, 5)
      |> Game.position_island(p_key, :s_shape, 5, 1)
      |> Game.position_island(p_key, :square, 5, 5)
      |> Game.set_islands(p_key)
    end)
  end

  @doc """
  Returns a Game with Players and Islands set.
  """
  def game_ready(opts \\ []) do
    check_options(opts)

    opts
    |> options()
    |> game_with_islands_set()
  end

  defp options(opts) do
    defaults = [
      player1: "Adam",
      player2: "Ginger"
    ]

    Keyword.merge(defaults, opts)
  end

  defp check_options(opts) do
    unless Keyword.keyword?(opts),
      do: raise(ArgumentError, message: "opts must be a keyword list")
  end
end
