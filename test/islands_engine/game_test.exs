defmodule IslandsEngine.GameTest do
  use ExUnit.Case
  doctest IslandsEngine.Game

  alias IslandsEngine.{Game, Coordinate}
  alias IslandsEngine.Fixtures.Games

  setup do
    {:ok, game} = Game.new("test-game")
    {:ok, game: game}
  end

  test "cannot add the same player", %{game: game} do
    error = Games.game_with_players("Adam", "Adam")

    assert error == {:error, "Player already added"}
  end

  test "addeds two diff players", %{game: game} do
    %Game{player1: p1, player2: p2} = Games.game_with_players("Adam", "Ginger")

    assert p1.name == "Adam"
    assert p2.name == "Ginger"
  end

  describe "position islands" do
    setup :add_players

    test "for players", %{game: game} do
      game =
        game
        |> Game.position_island(:player1, :square, 1, 1)
        |> Game.position_island(:player2, :square, 1, 1)

      assert Map.has_key?(game.player1.board, :square)
      assert Map.has_key?(game.player2.board, :square)
      assert :islands_not_set == game.rules.player1
      assert :islands_not_set == game.rules.player2
    end

    test "position island that overlaps fails", %{game: game} do
      game =
        game
        |> Game.position_island(:player1, :square, 1, 1)

      assert Map.has_key?(game.player1.board, :square)

      {:error, msg} = Game.position_island(game, :player1, :l_shape, 1, 1)
      assert msg == :overlapping_island
    end
  end

  describe "setting islands for players" do
    setup :add_players

    test "missing islands positioned, state is still :islands_not_set", %{
      game: game
    } do
      game =
        Enum.reduce([:player1, :player2], game, fn p, g ->
          g
          |> Game.position_island(p, :atoll, 1, 1)
          |> Game.position_island(p, :dot, 1, 4)
        end)

      assert {:error, :not_all_islands_positioned} =
               Game.set_islands(game, :player1)

      assert {:error, :not_all_islands_positioned} =
               Game.set_islands(game, :player2)
    end

    test "it changes the states for each player", %{game: game} do
      game =
        Enum.reduce([:player1, :player2], game, fn p, g ->
          g
          |> Game.position_island(p, :atoll, 1, 1)
          |> Game.position_island(p, :dot, 1, 4)
          |> Game.position_island(p, :l_shape, 1, 5)
          |> Game.position_island(p, :s_shape, 5, 1)
          |> Game.position_island(p, :square, 5, 5)
          |> Game.set_islands(p)
        end)

      assert :islands_set == game.rules.player1
      assert :islands_set == game.rules.player2
      assert :player1_turn == game.rules.state
    end
  end

  describe "guessing a coordinate" do
    test "adds a hit guess to the players board" do
      game = Games.game_with_islands_set()

      # Player 1 guess
      {hit_or_miss, win_no_win, forested?, game} =
        Game.guess_coordinate(game, :player1, Coordinate.new!(1, 1))

      assert hit_or_miss == :hit
      assert win_no_win == :no_win
      assert forested? == :none
      assert Enum.count(game.player1.guesses.hits) == 1
      assert Enum.empty?(game.player1.guesses.misses)

      # Player 2 guess
      {hit_or_miss, win_no_win, forested?, game} =
        Game.guess_coordinate(game, :player2, Coordinate.new!(1, 4))

      assert hit_or_miss == :hit
      assert win_no_win == :no_win
      assert forested? == :dot
      assert Enum.count(game.player2.guesses.hits) == 1
      assert Enum.empty?(game.player1.guesses.misses)
    end

    test "adds a miss guess to the players board" do
      game = Games.game_with_islands_set()

      # Player 1 guess
      {hit_or_miss, win_no_win, forested?, game} =
        Game.guess_coordinate(game, :player1, Coordinate.new!(8, 8))

      assert hit_or_miss == :miss
      assert win_no_win == :no_win
      assert forested? == :none
      assert Enum.empty?(game.player1.guesses.hits)
      assert Enum.count(game.player1.guesses.misses) == 1

      # Player 2 guess
      {hit_or_miss, win_no_win, forested?, game} =
        Game.guess_coordinate(game, :player2, Coordinate.new!(8, 8))

      assert hit_or_miss == :miss
      assert win_no_win == :no_win
      assert forested? == :none
      assert Enum.empty?(game.player1.guesses.hits)
      assert Enum.count(game.player2.guesses.misses) == 1
    end
  end

  defp add_players(%{game: game}) do
    game = Games.game_with_players("Adam", "Ginger")
    {:ok, game: game}
  end
end
