defmodule IslandsEngine.GameServerTest do
  use ExUnit.Case
  doctest IslandsEngine.GameServer

  alias IslandsEngine.{Guesses, GameServer}

  setup do
    {:ok, pid} = start_supervised({GameServer, "test-game"})
    {:ok, game_pid: pid}
  end

  test "sets an initial state", %{game_pid: pid} do
    %{game: game} = :sys.get_state(pid)

    assert %{} == game.player1.board
    assert %{} == game.player2.board
    assert Guesses.new() == game.player1.guesses
    assert Guesses.new() == game.player2.guesses
    assert game.name == "test-game"
  end

  test "add players", %{game_pid: pid} do
    assert :ok == GameServer.add_player(pid, "Adam")
    %{game: game} = :sys.get_state(pid)
    assert game.player1.name == "Adam"

    assert :ok == GameServer.add_player(pid, "Ginger")
    %{game: game} = :sys.get_state(pid)
    assert game.player2.name == "Ginger"

    assert game.rules.state == :players_set
  end

  test "fails to add player with same name", %{game_pid: pid} do
    assert :ok == GameServer.add_player(pid, "Adam")
    %{game: game} = :sys.get_state(pid)
    assert game.player1.name == "Adam"

    assert {:error, "Player already added"} ==
             GameServer.add_player(pid, "Adam")

    %{game: game} = :sys.get_state(pid)
    assert game.player2.name == ""
  end

  test "plays a game with player 2 winning", %{game_pid: pid} do
    # Setup board
    assert :ok == GameServer.add_player(pid, "Adam")
    assert :ok == GameServer.add_player(pid, "Ginger")

    Enum.each([:player1, :player2], fn p_key ->
      assert :ok == GameServer.position_island(pid, p_key, :atoll, 1, 1)
      assert :ok == GameServer.position_island(pid, p_key, :dot, 1, 4)
    end)

    assert {:error, :not_all_islands_positioned} ==
             GameServer.set_islands(pid, :player1)

    Enum.each([:player1, :player2], fn p_key ->
      assert :ok == GameServer.position_island(pid, p_key, :l_shape, 1, 5)
      assert :ok == GameServer.position_island(pid, p_key, :s_shape, 5, 1)
      assert :ok == GameServer.position_island(pid, p_key, :square, 5, 5)
    end)

    assert {:ok, _board = %{}} = GameServer.set_islands(pid, :player1)
    assert {:ok, _board = %{}} = GameServer.set_islands(pid, :player2)

    # Hit the dot for player 2 and make a miss for player1
    assert {:miss, :none, :no_win} =
             GameServer.guess_coordinate(pid, :player1, 10, 10)

    assert {:error, :invalid_state} =
             GameServer.guess_coordinate(pid, :player1, 1, 1)

    assert {:hit, :dot, :no_win} =
             GameServer.guess_coordinate(pid, :player2, 1, 4)

    # Uncomment to see the state and verify coords manually if needed
    # IO.inspect(:sys.get_state(pid))
    # Lets guess the rest of the hits.  Player 1 will not win because
    # previously the dot island was never hit
    hits = [
      atoll: [{1, 1}, {3, 1}, {1, 2}, {2, 2}, {3, 2}],
      l_shape: [{1, 5}, {3, 5}, {1, 6}, {3, 6}],
      s_shape: [{6, 1}, {5, 2}, {6, 2}, {5, 3}],
      square: [{5, 5}, {6, 5}, {5, 6}]
    ]

    Enum.each(hits, fn {_key, guesses} ->
      # IO.inspect("Key #{key}")

      Enum.each(guesses, fn {row, col} ->
        assert {:hit, _forested, _win_no_win} =
                 GameServer.guess_coordinate(pid, :player1, row, col)

        assert {:hit, _forested, _win_no_win} =
                 GameServer.guess_coordinate(pid, :player2, row, col)
      end)
    end)

    # Last guesses to win the game
    assert {:hit, :square, :no_win} =
             GameServer.guess_coordinate(pid, :player1, 6, 6)

    assert {:hit, :square, :win} =
             GameServer.guess_coordinate(pid, :player2, 6, 6)
  end
end
