defmodule IslandsEngine.RulesTest do
  use ExUnit.Case
  use ExUnitProperties

  doctest IslandsEngine.Rules

  alias IslandsEngine.Rules

  setup do
    {:ok, rules: Rules.new()}
  end

  test "happy path state transitions", %{rules: rules} do
    assert rules.state == :initialized

    # {command, new state}
    [
      # add players
      {:add_player, :adding_players},
      {:add_player, :players_set},
      # set islands
      {{:set_islands, :player1}, :players_set},
      {{:set_islands, :player2}, :player1_turn},
      # guesses
      {{:guess_coordinate, :player1}, :player2_turn},
      {{:guess_coordinate, :player2}, :player1_turn}
    ]
    |> Enum.reduce(rules, fn {cmd, state}, r ->
      {:ok, new_rules} = Rules.check(r, cmd)
      assert ^state = new_rules.state
      new_rules
    end)
  end

  test "moves from initial state", %{rules: rules} do
    assert rules.state == :initialized
    {:ok, rules} = Rules.check(rules, :add_player)
    assert :adding_players == rules.state
  end

  test "moves from :adding_players to :players_set", %{rules: rules} do
    {:ok, rules} = Rules.check(rules, :add_player)
    {:ok, rules} = Rules.check(rules, :add_player)
    assert :players_set == rules.state
  end

  describe "setting islands" do
    test "set islands", %{rules: rules} do
      {:ok, rules} =
        Rules.check(
          %Rules{rules | state: :players_set},
          {:set_islands, :player1}
        )

      assert(rules.state == :players_set)
      {:ok, rules} = Rules.check(rules, {:set_islands, :player1})
      assert(rules.state == :players_set)
      {:ok, rules} = Rules.check(rules, {:set_islands, :player2})
      assert(rules.state == :player1_turn)
    end

    test "set islands fails when when invalid prev state", %{rules: rules} do
      rules = %Rules{rules | state: :player1_turn}

      # {cmd, next_state}
      commands = [
        {:set_islands, :player2},
        :add_layer,
        {:position_islands, :player1},
        {:set_islands, :player2}
      ]

      Enum.each(commands, fn command ->
        assert {:error, :invalid_state} == Rules.check(rules, command)
      end)
    end
  end

  describe "switch turns" do
    test "switch turns", %{rules: rules} do
      rules = %{rules | state: :player1_turn}
      {:ok, rules} = Rules.check(rules, {:guess_coordinate, :player1})
      assert :player2_turn == rules.state

      {:ok, rules} = Rules.check(rules, {:guess_coordinate, :player2})
      assert :player1_turn == rules.state
    end
  end
end
