defmodule IslandsEngine.IslandTest do
  use ExUnit.Case
  doctest IslandsEngine.Island

  alias IslandsEngine.{Island, Coordinate}

  describe "building an island" do
    setup %{island_type: ist, start_coord: sc} do
      {:ok, %Island{coordinates: coords}} =
        Island.new(ist, sc)

      {:ok, coords: coords}
    end

    @tag island_type: :square
    @tag start_coord: Coordinate.new!(2, 2)
    test "generate a square island", %{coords: coords} do
      assert MapSet.size(coords) == 4
      assert MapSet.member?(coords, Coordinate.new!(2, 2))
      assert MapSet.member?(coords, Coordinate.new!(2, 3))
      assert MapSet.member?(coords, Coordinate.new!(3, 2))
      assert MapSet.member?(coords, Coordinate.new!(3, 3))
    end

    @tag island_type: :atoll
    @tag start_coord: Coordinate.new!(2, 2)
    test "generate a :atoll island", %{coords: coords} do
      assert MapSet.size(coords) == 5
      assert MapSet.member?(coords, Coordinate.new!(2, 2))
      assert MapSet.member?(coords, Coordinate.new!(4, 2))
      assert MapSet.member?(coords, Coordinate.new!(2, 3))
      assert MapSet.member?(coords, Coordinate.new!(3, 3))
      assert MapSet.member?(coords, Coordinate.new!(4, 3))
    end

    @tag island_type: :dot
    @tag start_coord: Coordinate.new!(2, 2)
    test "generate a :dot island", %{coords: coords} do
      assert MapSet.size(coords) == 1
      assert MapSet.member?(coords, Coordinate.new!(2, 2))
    end

    @tag island_type: :l_shape
    @tag start_coord: Coordinate.new!(2, 2)
    test "generate a :l_shape island", %{coords: coords} do
      assert MapSet.size(coords) == 4
      assert MapSet.member?(coords, Coordinate.new!(2, 2))
      assert MapSet.member?(coords, Coordinate.new!(4, 2))
      assert MapSet.member?(coords, Coordinate.new!(2, 3))
      assert MapSet.member?(coords, Coordinate.new!(4, 3))
    end

    @tag island_type: :s_shape
    @tag start_coord: Coordinate.new!(2, 2)
    test "generate a :s_shape island", %{coords: coords} do
      assert MapSet.size(coords) == 4
      assert MapSet.member?(coords, Coordinate.new!(3, 2))
      assert MapSet.member?(coords, Coordinate.new!(3, 2))
      assert MapSet.member?(coords, Coordinate.new!(3, 3))
      assert MapSet.member?(coords, Coordinate.new!(2, 4))
    end
  end
end
