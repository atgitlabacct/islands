defmodule IslandsEngine.BoardTest do
  use ExUnit.Case, async: true
  doctest IslandsEngine.Board

  alias IslandsEngine.{Board, Island, Coordinate}

  test "position same type of island replaces original" do
    square = Island.new!(:square, Coordinate.new!(1, 1))
    board = Board.position_island(Board.new(), square)
    assert Map.has_key?(board, :square)

    square = Island.new!(:square, Coordinate.new!(2, 1))
    board = Board.position_island(board, square)
    assert Map.has_key?(board, :square)
  end

  test "errors out on overlapping islands" do
    square = Island.new!(:square, Coordinate.new!(1, 1))
    board = Board.position_island(Board.new(), square)
    assert Map.has_key?(board, :square)

    l_shape = Island.new!(:l_shape, Coordinate.new!(1, 1))
    {:error, msg} = Board.position_island(board, l_shape)
    assert msg == :overlapping_island
  end
end
