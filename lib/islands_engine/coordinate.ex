defmodule IslandsEngine.Coordinate do
  @moduledoc """
  Coordinate(s) are the basic unit for players's boards and of islands as well.
  A coordinate is defined by a row and a col (column).
  """

  alias __MODULE__

  @board_range 1..10

  @enforce_keys [:row, :col]
  defstruct [:row, :col]

  @type t :: %Coordinate{row: Integer, col: Integer}

  @doc """
  Generate a new coordinate

  ## Example
    iex> alias IslandsEngine.Coordinate
    iex> Coordinate.new(1,1)
    {:ok, %IslandsEngine.Coordinate{row: 1, col: 1}}
    iex> Coordinate.new(5,1)
    {:ok, %IslandsEngine.Coordinate{row: 5, col: 1}}
  """
  @spec new(row :: integer(), col :: integer()) ::
          {:ok, %Coordinate{}}
          | {:error, atom()}
  def new(row, col) when row in @board_range and col in @board_range do
    coord = %Coordinate{row: row, col: col}
    {:ok, coord}
  end

  def new(_row, _col), do: {:error, :invalid_coordinate}

  @doc """
  Generate a new coordinate or returns an error tuple

  ## Example
    iex> alias IslandsEngine.Coordinate
    iex> Coordinate.new!(1,1)
    %IslandsEngine.Coordinate{row: 1, col: 1}
    iex> Coordinate.new!(-11,1)
    {:error, :invalid_coordinate}
  """
  @spec new!(row :: integer(), col :: integer()) ::
          %Coordinate{} | {:error, atom()}
  def new!(row, col) when row in @board_range and col in @board_range do
    {:ok, coord} = new(row, col)
    coord
  end

  def new!(_row, _col), do: {:error, :invalid_coordinate}
end
