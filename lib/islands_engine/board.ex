defmodule IslandsEngine.Board do
  @moduledoc """
  The board plays a dual role.  First it knows about and can address all
  islands.  It can delegate function calls do to them individually or as a
  group. This makes the board an orchestrator as well as an interface for
  actions that involve islands.

  We need to handle positioning islands, ensuring all islands are positioned,
  and guessing coordinates

  A Board is a map with a the key being the type of island, like a :square or
  and :l_shape.  The value of that key would be the actual island with
  Coordinates.
  """

  alias IslandsEngine.{Board, Island}

  @type t :: map()

  @spec new() :: %{}
  def new() do
    %{}
  end

  @doc """
  Checks to see if Islands overlap on the board.  If they do the island can't
  be positioned and an error returned.

  ## Example
    iex> alias IslandsEngine.{Board, Fixtures.Islands}
    iex> board = Board.new
    iex> board = Board.position_island(board, Islands.square())
    iex> Board.position_island(board, Islands.l_shape())
    {:error, :overlapping_island}
    iex> %{l_shape: _, square: _} = Board.position_island(board,
    ...> Islands.l_shape(6,6))
    iex> :ok # verify above statement
    :ok
  """
  @spec position_island(board :: map(), island :: %Island{}) :: map()
  def position_island(board, %Island{type: key} = island) do
    case overlaps_existing_island?(board, key, island) do
      true -> {:error, :overlapping_island}
      false -> Map.put(board, key, island)
    end
  end

  defp overlaps_existing_island?(board, new_key, new_island) do
    # If we pass in the same key for an island we simply replace the location
    # of the island with the new passed in island
    Enum.any?(board, fn {key, island} ->
      key != new_key and Island.overlaps?(island, new_island)
    end)
  end

  @doc """
  Verify that all island types have been positioned on the board.

  ## Example
    iex> alias IslandsEngine.{Board, Island, Fixtures.Islands}
    iex> board = Board.position_island(Board.new, Islands.square())
    iex> Board.all_islands_positioned?(board)
    false
  """
  def all_islands_positioned?(board) do
    Enum.all?(Island.types(), &Map.has_key?(board, &1))
  end

  @doc """
  Determine if a guess was a hit or miss and if you won.

  ## Example
    iex> alias IslandsEngine.{Board, Island, Coordinate}
    iex> {:ok, coordinate} = Coordinate.new(2,2)
    iex> island = Island.new!(:square, coordinate)
    iex> board = Board.position_island(Board.new, island)
    iex> {:hit, :none, :no_win, %{} = _} = Board.guess(board, coordinate)
    iex> :ok # Really testing the above statement
    :ok
    iex> {:ok, coordinate} = Coordinate.new(1,1)
    iex> {:miss, :none, :no_win, %{} = _} = Board.guess(board, coordinate)
    iex> :ok # Really testing the above statement
    :ok
  """
  @spec guess(board :: map, coordinate :: Coordinate) ::
          {:miss | :hit, Island.t_island() | :none, :no_win | :win, Board}
  def guess(board, coordinate) do
    board
    |> check_all_islands(coordinate)
    |> guess_response(board)
  end

  defp check_all_islands(board, coordinate) do
    Enum.find_value(board, :miss, fn {key, island} ->
      case Island.guess(island, coordinate) do
        {:hit, island} -> {key, island}
        :miss -> false
      end
    end)
  end

  defp guess_response({key, island}, board) do
    board = %{board | key => island}
    {:hit, forest_check(board, key), win_check(board), board}
  end

  defp guess_response(:miss, board) do
    {:miss, :none, :no_win, board}
  end

  defp forest_check(board, key) do
    case forested?(board, key) do
      true -> key
      false -> :none
    end
  end

  defp forested?(board, key) do
    board
    |> Map.fetch!(key)
    |> Island.forested?()
  end

  defp win_check(board) do
    case all_forested?(board) do
      true -> :win
      false -> :no_win
    end
  end

  defp all_forested?(board) do
    Enum.all?(board, fn {_key, island} ->
      Island.forested?(island)
    end)
  end
end
