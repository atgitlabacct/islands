defmodule IslandsEngine.Game do
  @moduledoc """
  Supports the actions of the game.
  """
  alias IslandsEngine.{Game, Board, Guesses, Rules, Island, Coordinate}

  defstruct name: "", player1: %{}, player2: %{}, rules: Rules.new()

  @type t :: %IslandsEngine.Game{
          name: nil | String.t(),
          player1: nil | map(),
          player2: nil | map(),
          rules: Rules.t()
        }

  @type t_players :: :player1 | :player2
  @players [:player1, :player2]

  @type t_guess_result ::
          {:hit | :miss, :no_win | :win, Island.t_island() | :none, Game.t()}
          | :error
          | {:error, atom()}

  @doc """
  Creates a new Game struct
  """
  @spec new(name :: String.t()) :: {:ok, Game.t()}
  def new(name) when is_binary(name) do
    player1 = %{name: "", board: Board.new(), guesses: Guesses.new()}
    player2 = %{name: "", board: Board.new(), guesses: Guesses.new()}

    {:ok, %Game{name: name, player1: player1, player2: player2}}
  end

  @doc """
  Add a player to the name

  ## Examples
    iex> alias IslandsEngine.Game
    iex> {:ok, game} = Game.new("test-game")
    iex> {:ok, game} = Game.add_player(game, "Adam")
    iex> game.player1.name
    "Adam"
    iex> {:ok, game} = Game.add_player(game, "Ginger")
    iex> game.player2.name
    "Ginger"
  """
  @spec add_player(game :: Game.t(), name: String.t()) ::
          {:ok, Game.t()} | :error
  def add_player(%Game{player1: %{name: ""}} = game, name)
      when is_binary(name) do
    with({:ok, rules} <- Rules.check(game.rules, :add_player)) do
      game = put_in(game.player1.name, name)
      {:ok, %{game | rules: rules}}
    else
      _ -> :error
    end
  end

  def add_player(%Game{player2: %{name: ""}} = game, name)
      when is_binary(name) do
    with({:ok, rules} <- Rules.check(game.rules, :add_player)) do
      if game.player1.name == name do
        {:error, "Player already added"}
      else
        game = put_in(game.player2.name, name)
        {:ok, %{game | rules: rules}}
      end
    else
      _ -> :error
    end
  end

  def add_player(%Game{}, _) do
    {:error, :invalid_player_or_already_added}
  end

  @doc """
  Adds a player to the game and returns the raw game with no tuple.
  """
  @spec add_player!(game :: Game.t(), name: String.t()) :: Game.t() | :error
  def add_player!(game, name) do
    with({:ok, new_game} <- Game.add_player(game, name)) do
      new_game
    else
      {:error, _msg} = error -> error
      _ -> :error
    end
  end

  @doc """
  Make a guess for the given player key.
  """
  @spec guess_coordinate(
          game :: Game.t(),
          player_key :: t_players(),
          coordinate :: Coordinate.t()
        ) :: t_guess_result
  def guess_coordinate(game, player_key, coordinate)
      when player_key in [:player1, :player2] do
    opponent_key = opponent(player_key)
    opponent_board = player_board(game, opponent_key)

    with(
      {:ok, rules} <- Rules.check(game.rules, {:guess_coordinate, player_key}),
      {hit_or_miss, forested_island, win_status, opponent_board} <-
        Board.guess(opponent_board, coordinate),
      {:ok, rules} <- Rules.check(rules, {:win_check, win_status})
    ) do
      game =
        game
        |> update_board(opponent_key, opponent_board)
        |> update_guesses(player_key, hit_or_miss, coordinate)
        |> update_rules(rules)

      {hit_or_miss, win_status, forested_island, game}
    else
      :error -> :error
      {:error, msg} -> {:error, msg}
    end
  end

  defp update_guesses(game, :player1, hit_or_miss, coordinate) do
    update_in(game.player1.guesses, fn guesses ->
      Guesses.add(guesses, hit_or_miss, coordinate)
    end)
  end

  defp update_guesses(game, :player2, hit_or_miss, coordinate) do
    update_in(game.player2.guesses, fn guesses ->
      Guesses.add(guesses, hit_or_miss, coordinate)
    end)
  end

  @doc """
  Postions the islands in the game
  """
  @spec position_island(
          game :: Game.t(),
          player :: Rules.t_players(),
          key :: Island.t_island(),
          row :: integer(),
          col :: integer()
        ) :: Game.t() | :error | {:error, atom()}
  def position_island(game, player, key, row, col) do
    board = player_board(game, player)

    with(
      {:ok, rules} <- Rules.check(game.rules, {:position_islands, player}),
      {:ok, coordinate} <- Coordinate.new(row, col),
      {:ok, island} <- Island.new(key, coordinate),
      board = %{} <- Board.position_island(board, island)
    ) do
      game |> update_board(player, board) |> update_rules(rules)
    else
      :error -> :error
      {:error, msg} -> {:error, msg}
    end
  end

  @doc """
  Changes the state if valid so that players can no longer position there
  islands and sets the game state to the players turn.

  ## Examples
    iex> alias IslandsEngine.{Game}
    iex> {:ok, game} = Game.new("Test Game")
    iex> {:ok, game} = Game.add_player(game, "Adam")
    iex> {:ok, game} = Game.add_player(game, "Ginger")
    iex> Game.set_islands(game, :player1)
    {:error, :not_all_islands_positioned}
    iex> game = Game.position_island(game, :player1, :atoll, 1, 1)
    iex> game = Game.position_island(game, :player1, :dot, 1, 4)
    iex> game = Game.position_island(game, :player1, :l_shape, 1, 5)
    iex> game = Game.position_island(game, :player1, :s_shape, 5, 1)
    iex> game = Game.position_island(game, :player1, :square, 5, 5)
    iex> game = Game.set_islands(game, :player1)
    iex> game.rules.player1
    :islands_set
    iex> game.rules.player2
    :islands_not_set
  """
  @spec set_islands(game :: Game.t(), players :: t_players) ::
          Game.t() | {:error, atom()}
  def set_islands(game = %Game{}, player) when player in @players do
    board = player_board(game, player)

    with {:ok, rules} <- Rules.check(game.rules, {:set_islands, player}),
         true <- Board.all_islands_positioned?(board) do
      update_rules(game, rules)
    else
      false -> {:error, :not_all_islands_positioned}
      {:error, msg} -> {:error, msg}
    end
  end

  def player_board(game, player), do: Map.get(game, player).board

  ############################################################################
  # Shared privates
  ############################################################################
  defp update_board(game, player, board) do
    Map.update!(game, player, fn player ->
      %{player | board: board}
    end)
  end

  defp update_rules(game, rules), do: %Game{game | rules: rules}

  defp opponent(:player1), do: :player2
  defp opponent(:player2), do: :player1
end
