defmodule IslandsEngine.GameServer do
  @moduledoc """
  Wrapper around the Game module to support Genserver callbacks.
  """
  use GenServer

  alias IslandsEngine.{Island, Game, Coordinate, Board}

  @players [:player1, :player2]

  ############################################################
  ## Client Interface
  @doc """
  Starts the process
  """
  def start_link(name) when is_binary(name) do
    GenServer.start_link(__MODULE__, name, [])
  end

  @doc """
  Add a player to the game
  """
  @spec add_player(game :: pid(), name :: String.t()) :: any()
  def add_player(game, name) when is_binary(name) do
    GenServer.call(game, {:add_player, name})
  end

  @doc """
  Guess a coordinate
  """
  @spec guess_coordinate(
          game :: pid(),
          player_key :: Game.t_players(),
          row :: Integer,
          col :: Integer
        ) :: Game.t_guess_result()
  def guess_coordinate(game, player, row, col) when player in @players do
    GenServer.call(game, {:guess_coordinate, player, row, col})
  end

  @spec position_island(
          game_pid :: pid(),
          player :: :player1 | :player2,
          key :: Island.t_island(),
          row :: integer(),
          col :: integer()
        ) :: any()
  def position_island(game_pid, player, key, row, col)
      when player in @players do
    GenServer.call(game_pid, {:position_island, player, key, row, col})
  end

  @spec set_islands(pid :: pid(), player :: :player1 | :player2) ::
          {:ok, Board.t()} | {:error, :not_all_islands_positioned}
  def set_islands(pid, player) do
    GenServer.call(pid, {:set_islands, player})
  end

  ############################################################
  ## Callbacks
  def init(name) do
    {:ok, game} = Game.new(name)
    {:ok, %{game: game}}
  end

  def handle_call({:add_player, name}, _from, state) do
    with({:ok, game} <- update_player_name(state, name)) do
      reply_success(%{state | game: game}, :ok)
    else
      :error -> {:reply, :error, state}
      {:error, _msg} = msg -> {:reply, msg, state}
    end
  end

  def handle_call({:position_island, player, key, row, col}, _from, state) do
    with(
      game = %Game{} <- Game.position_island(state.game, player, key, row, col)
    ) do
      reply_success(%{state | game: game}, :ok)
    else
      msg = :error -> {:reply, msg, state}
      msg = {:error, :invalid_coordinate} -> {:reply, msg, state}
      msg = {:error, :invalid_island_type} -> {:reply, msg, state}
    end
  end

  def handle_call({:set_islands, player}, _from, state) do
    with(game = %Game{} <- Game.set_islands(state.game, player)) do
      board = get_in(game, [Access.key(player), :board])
      reply_success(%{state | game: game}, {:ok, board})
    else
      {:error, msg} -> {:reply, {:error, msg}, state}
    end
  end

  def handle_call({:guess_coordinate, player_key, row, col}, _from, state) do
    with(
      {:ok, coord} <- Coordinate.new(row, col),
      {hit_miss, win_no_win, forested_island, game} <-
        Game.guess_coordinate(state.game, player_key, coord)
    ) do
      reply_success(
        %{state | game: game},
        {hit_miss, forested_island, win_no_win}
      )
    else
      {:error, msg} ->
        {:reply, {:error, msg}, state}

      _ ->
        {:reply, {:error, :unknown_error}, state}
    end
  end

  defp update_player_name(state, name) do
    Game.add_player(state.game, name)
  end

  defp reply_success(state, reply), do: {:reply, reply, state}
end
