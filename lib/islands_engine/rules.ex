defmodule IslandsEngine.Rules do
  @moduledoc """
  Manages the custom state machine
  """

  alias __MODULE__

  @typep t_win_or_not :: :win | :no_win
  @typep t_game_states ::
           :initialized
           | :adding_players
           | :players_set
           | :game_over
           | :player1_turn
           | :player2_turn
  @typep t_player_states :: :islands_not_set | :islands_set
  @typep t_check_events ::
           :add_player
           | {:position_islands, t_players}
           | {:set_islands, t_players}
           | {:guess_coordinate, t_players}
           | {:win_check, t_win_or_not}
  @type t_players :: :player1 | :player2
  @type t :: %Rules{
          state: t_game_states,
          player1: t_player_states,
          player2: t_player_states
        }

  @type t_errors :: {:error, :islands_already_set} | {:error, :invalid_state}

  @enforce_keys [:state, :player1, :player2]
  defstruct state: :initialized,
            player1: :islands_not_set,
            player2: :islands_not_set

  @doc """
  Creates a new Rules struct
  """
  @spec new() :: Rules.t()
  def new(),
    do: %Rules{
      state: :initialized,
      player1: :islands_not_set,
      player2: :islands_not_set
    }

  @doc """
  Provides the function to 'whitelist' the states of our application.

  A normal flow would be

  TODO: Describe flow

  ## Examples
    iex> alias IslandsEngine.Rules
    iex> {:ok, rules} = Rules.check(Rules.new, :add_player)
    iex> rules.state
    :adding_players

    iex> alias IslandsEngine.Rules
    iex> Rules.check(%Rules{Rules.new() | state: :players_set}, :add_player)
    {:error, :invalid_state}
  """
  @spec check(rules :: Rules.t(), t_check_events) :: {:ok, %Rules{}} | t_errors
  def check(%Rules{state: :initialized} = rules, :add_player) do
    {:ok, %Rules{rules | state: :adding_players}}
  end

  def check(%Rules{state: :adding_players} = rules, :add_player) do
    {:ok, %Rules{rules | state: :players_set}}
  end

  def check(%Rules{state: :players_set} = rules, {:position_islands, player}) do
    case Map.fetch!(rules, player) do
      :islands_set -> {:error, :islands_already_set}
      :islands_not_set -> {:ok, rules}
    end
  end

  def check(%Rules{state: :players_set} = rules, {:set_islands, player}) do
    rules = Map.put(rules, player, :islands_set)

    case both_players_islands_set?(rules) do
      true -> {:ok, %Rules{rules | state: :player1_turn}}
      false -> {:ok, rules}
    end
  end

  def check(%Rules{state: :player1_turn} = rules, {:guess_coordinate, :player1}) do
    {:ok, %Rules{rules | state: :player2_turn}}
  end

  def check(%Rules{state: :player2_turn} = rules, {:guess_coordinate, :player2}) do
    {:ok, %Rules{rules | state: :player1_turn}}
  end

  def check(%Rules{} = rules, {:win_check, win_or_not}) do
    case win_or_not do
      :no_win -> {:ok, rules}
      :win -> {:ok, %Rules{rules | state: :game_over}}
    end
  end

  def check(state, action), do: {:error, :invalid_state}

  defp both_players_islands_set?(rules) do
    rules.player1 == :islands_set && rules.player2 == :islands_set
  end
end
