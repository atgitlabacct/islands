defmodule IslandsEngine.Island do
  @moduledoc """
  Islands come in four difference shapes: :atoll, :dot, :l_shape, :s_shape and
  :square.  Players position islands on the board and players try to guess the
  location of the island.

  Islands are made up of groups of coordinates.  One thing we need to keep in
  is we want to tell if an island is 'forested' which means all coordinates of
  the island have been hit.

  We define an Island to have :coordinates and :hit_coordinates which are
  stored as MapSet.  This allows comparisson of the two coordinate sets easily.
  """
  alias IslandsEngine.{Coordinate, Island}

  @enforce_keys [:coordinates, :hit_coordinates]
  @island_types [:atoll, :dot, :l_shape, :s_shape, :square]
  defstruct [:coordinates, :hit_coordinates, :type]

  @type t_island :: :square | :atoll | :dot | :l_shape | :s_shape

  @doc """
  Creates a new %Island{} given the type and starting %Coordinate{} location
  """
  @spec new(type :: Island.t_island(), upper_left :: %Coordinate{}) ::
          {:ok, %Island{}} | {:error, atom}
  def new(type, upper_left) do
    with [_ | _] = offsets <- offsets(type),
         %MapSet{} = coordinates <- add_coordinates(offsets, upper_left) do
      {:ok,
       %Island{
         coordinates: coordinates,
         hit_coordinates: MapSet.new(),
         type: type
       }}
    else
      error -> error
    end
  end

  @doc """
  Creates a new %Island{} given the type and starting %Coordinate{} location
  """
  @spec new!(type :: Island.t_island(), upper_left :: %Coordinate{}) ::
          %Island{} | {:error, atom}
  def new!(type, upper_left) do
    with {:ok, island} <- Island.new(type, upper_left) do
      island
    else
      error -> error
    end
  end

  defp add_coordinates(offsets, upper_left) do
    Enum.reduce_while(offsets, MapSet.new(), fn offset, acc ->
      add_coordinate(acc, upper_left, offset)
    end)
  end

  defp add_coordinate(
         coordinates,
         %Coordinate{row: row, col: col},
         {row_offset, col_offsetj}
       ) do
    case Coordinate.new(row + row_offset, col + col_offsetj) do
      {:ok, coordinate} ->
        {:cont, MapSet.put(coordinates, coordinate)}

      {:error, :invalid_coordinate} ->
        {:halt, {:error, :invalid_coordinate}}
    end
  end

  defp offsets(:square), do: [{0, 0}, {0, 1}, {1, 0}, {1, 1}]
  defp offsets(:atoll), do: [{0, 0}, {0, 1}, {1, 1}, {2, 0}, {2, 1}]
  defp offsets(:dot), do: [{0, 0}]
  defp offsets(:l_shape), do: [{0, 0}, {0, 1}, {2, 0}, {2, 1}]
  defp offsets(:s_shape), do: [{0, 1}, {0, 2}, {1, 0}, {1, 1}]
  defp offsets(_), do: {:error, :invalid_island_type}

  @doc """
  Checks to see if two islands overlap with each other

  ## Example
    iex> alias IslandsEngine.{Coordinate, Island}
    iex> {:ok, square_coord} = Coordinate.new(1,1)
    iex> {:ok, square} = Island.new(:square, square_coord)
    iex> {:ok, l_shape_coord} = Coordinate.new(1,1)
    iex> {:ok, l_shape} = Island.new(:l_shape, l_shape_coord)
    iex> Island.overlaps?(square, l_shape)
    true
  """
  @spec overlaps?(%Island{}, %Island{}) :: boolean()
  def overlaps?(existing_island, new_island) do
    not MapSet.disjoint?(existing_island.coordinates, new_island.coordinates)
  end

  @doc """
  Checks to see if the guess (Coordinate) given hits an island.

  ## Examples

    iex> alias IslandsEngine.{Coordinate, Island, Fixtures.Islands}
    iex> {:ok, guess} = Coordinate.new(1,2)
    iex> {:hit, island} = Island.guess(Islands.square(1,1), guess)
    iex> island.hit_coordinates
    #MapSet<[%IslandsEngine.Coordinate{col: 2, row: 1}]>

    iex> alias IslandsEngine.{Coordinate, Island, Fixtures.Islands}
    iex> {:ok, guess} = Coordinate.new(9,9)
    iex> Island.guess(Islands.square(), guess)
    :miss
  """
  @spec guess(%Island{}, %Coordinate{}) :: {:hit, %Island{}} | :miss
  def guess(%Island{} = island, %Coordinate{} = coordinate) do
    case MapSet.member?(island.coordinates, coordinate) do
      true ->
        hit_coordinates = MapSet.put(island.hit_coordinates, coordinate)
        {:hit, %{island | hit_coordinates: hit_coordinates}}

      false ->
        :miss
    end
  end

  @doc """
  Checks to see if an island is completely forested

  ## Examples

    iex> alias IslandsEngine.{Coordinate, Island, Fixtures.Islands}
    iex> {:ok, guess} = Coordinate.new(1,1)
    iex> {:hit, island} = Island.guess(Islands.square(1,1), guess)
    iex> Island.forested?(island)
    false
    iex> {:ok, guess} = Coordinate.new(1,2)
    iex> {:hit, island} = Island.guess(island, guess)
    iex> Island.forested?(island)
    false
    iex> {:ok, guess} = Coordinate.new(2,1)
    iex> {:hit, island} = Island.guess(island, guess)
    iex> Island.forested?(island)
    false
    iex> {:ok, guess} = Coordinate.new(2,2)
    iex> {:hit, island} = Island.guess(island, guess)
    iex> Island.forested?(island)
    true
  """
  @spec forested?(%Island{}) :: boolean()
  def forested?(%Island{} = island) do
    MapSet.equal?(island.coordinates, island.hit_coordinates)
  end

  @doc """
  Returns the island types
  """
  def types(), do: @island_types
end
