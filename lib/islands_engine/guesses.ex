defmodule IslandsEngine.Guesses do
  @moduledoc """
  Guesses is just a set of Coordinates stored in a MapSet called hits and misses.
  We are using a MapSet here to ensure uniquess amongst the guesses.
  """

  alias IslandsEngine.{Guesses, Coordinate}

  @enforce_keys [:hits, :misses]
  @typep t_guess :: :hit | :miss

  defstruct [:hits, :misses]

  # @spec new() :: {:ok, %Guesses{hits: %MapSet{}, misses: %MapSet{}}}
  # def new(), do: {:ok, %Guesses{hits: MapSet.new(), misses: MapSet.new()}}
  #
  @doc """
  Builds a Guesses struct.

  Example:
    iex> IslandsEngine.Guesses.new()
    %IslandsEngine.Guesses{hits: %MapSet{}, misses: %MapSet{}}
  """
  @spec new() :: %Guesses{hits: %MapSet{}, misses: %MapSet{}}
  def new(), do: %Guesses{hits: MapSet.new(), misses: MapSet.new()}

  @doc """
  Adds a guess to hit or miss

  ## Example
  iex> alias IslandsEngine.{Guesses, Coordinate}
  iex> {:ok, coord} = Coordinate.new(1,1)
  iex> %Guesses{hits: hits, misses: _} = Guesses.add(Guesses.new, :hit, coord)
  iex> hits
  #MapSet<[%IslandsEngine.Coordinate{col: 1, row: 1}]>
  iex> %Guesses{hits: _, misses: misses} = Guesses.add(Guesses.new, :miss, coord)
  iex> misses 
  #MapSet<[%IslandsEngine.Coordinate{col: 1, row: 1}]>
  """
  @spec add(guesses :: %Guesses{}, t_guess, coordinate :: %Coordinate{}) ::
          %Guesses{}
  def add(guesses, :hit, coordinate) do
    update_in(guesses.hits, &MapSet.put(&1, coordinate))
  end

  def add(guesses, :miss, coordinate) do
    update_in(guesses.misses, &MapSet.put(&1, coordinate))
  end
end
